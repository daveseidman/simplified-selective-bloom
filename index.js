import { Scene, WebGLRenderer, PerspectiveCamera, AmbientLight, MeshBasicMaterial, Color, Vector2, ShaderMaterial, IcosahedronGeometry, Mesh } from 'three';
import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer.js';
import { RenderPass } from 'three/examples/jsm/postprocessing/RenderPass.js';
import { ShaderPass } from 'three/examples/jsm/postprocessing/ShaderPass.js';
import { UnrealBloomPass } from 'three/examples/jsm/postprocessing/UnrealBloomPass.js';

const darkMaterial = new MeshBasicMaterial( { color: "black" } );

const renderer = new WebGLRenderer( { antialias: true } );
renderer.setPixelRatio( window.devicePixelRatio );
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );

const scene = new Scene();

const camera = new PerspectiveCamera( 40, window.innerWidth / window.innerHeight, 1, 200 );
camera.position.set( 0, 0, 20 );
camera.lookAt( 0, 0, 0 );

scene.add( new AmbientLight( 0x404040 ) );

const renderPass = new RenderPass( scene, camera );
const bloomPass = new UnrealBloomPass( new Vector2( window.innerWidth, window.innerHeight ), 5, 1, 0.1 );
const composer = new EffectComposer( renderer );
composer.renderToScreen = false;
composer.addPass( renderPass );
composer.addPass( bloomPass );
const finalPass = new ShaderPass(
  new ShaderMaterial( {
    uniforms: { baseTexture: { value: null }, bloomTexture: { value: composer.renderTarget2.texture } },
    vertexShader: `varying vec2 vUv; void main() { vUv = uv; gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 ); }`,
    fragmentShader: `uniform sampler2D baseTexture;	uniform sampler2D bloomTexture; varying vec2 vUv; void main() { gl_FragColor = ( texture2D( baseTexture, vUv ) + vec4( 1.0 ) * texture2D( bloomTexture, vUv ) ); }`,
  } ), "baseTexture"
);

const finalComposer = new EffectComposer( renderer );
finalComposer.addPass( renderPass );
finalComposer.addPass( finalPass );

const glowObjects = [];

const geometry = new IcosahedronGeometry( 1, 15 );

for ( let i = 0; i < 50; i ++ ) {
  const color = new Color();
  color.setHSL( Math.random(), 0.7, Math.random() * 0.2 + 0.05 );

  const material = new MeshBasicMaterial( { color: color } );
  material.name = Math.random() > 0.3 ? 'neon' : 'normal';
  const sphere = new Mesh( geometry, material );
  sphere.position.x = Math.random() * 10 - 5;
  sphere.position.y = Math.random() * 10 - 5;
  sphere.position.z = Math.random() * 10 - 5;
  sphere.position.normalize().multiplyScalar( Math.random() * 4.0 + 2.0 );
  sphere.scale.setScalar( Math.random() * Math.random() + 0.5 );
  scene.add( sphere );
  sphere.originalMaterial = material;
  if(material.name === 'neon') glowObjects.push(sphere);
}

render();


function render() {
  scene.rotation.y += .005;

  glowObjects.forEach(obj => { obj.material = darkMaterial; })
  composer.render();
  glowObjects.forEach(obj => { obj.material = obj.originalMaterial; })
  finalComposer.render();

  requestAnimationFrame(render);
}
